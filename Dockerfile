FROM php:fpm-alpine
LABEL maintainer="Nik Kyriakidis <nik.k2580@gmail.com>"

## run general stuff we need here
RUN apk update && apk upgrade && apk add bash git

# Install PHP extensions
RUN apk add freetds freetype icu libintl libldap libjpeg libmcrypt libpng libpq libwebp curl-dev freetds-dev \
    freetype-dev \
    gettext-dev \
    icu-dev \
    jpeg-dev \
    libmcrypt-dev \
    libpng-dev \
    libwebp-dev \
    libxml2-dev \
    openldap-dev \
    postgresql-dev

# Configure extensions
RUN docker-php-ext-configure gd --with-jpeg-dir=usr/ --with-freetype-dir=usr/ --with-webp-dir=usr/ \
    && docker-php-ext-configure ldap --with-libdir=lib/\ 
    && docker-php-ext-configure pdo_dblib --with-libdir=lib/

# Download mongo extension
RUN /usr/local/bin/pecl download mongodb && \
    tar -C /usr/src/php/ext -xf mongo*.tgz && \
    rm mongo*.tgz && \
    mv /usr/src/php/ext/mongo* /usr/src/php/ext/mongodb

### do the php exts
RUN docker-php-ext-install \
    curl \
    exif \
    gd \
    gettext \
    intl \
    ldap \
    mongodb \
    pdo_dblib \
    pdo_mysql \
    pdo_pgsql \
    xmlrpc \
    zip

# Download trusted certs 
RUN mkdir -p /etc/ssl/certs && update-ca-certificates

# Install composer
RUN php -r "readfile('https://getcomposer.org/installer');" | php && \
   mv composer.phar /usr/bin/composer && \
   chmod +x /usr/bin/composer

## clean stuff up after doing the PHP installs
RUN apk del curl-dev \
    freetds-dev \
    freetype-dev \
    gettext-dev \
    icu-dev \
    jpeg-dev \
    libmcrypt-dev \
    libpng-dev \
    libwebp-dev \
    libxml2-dev \
    openldap-dev \
    postgresql-dev

# Download and install NodeJS
RUN apk add --update nodejs nodejs-npm
RUN npm i -g yarn

RUN mkdir -p /etc/ssl/certs && update-ca-certificates

WORKDIR /var/www
